package kr.ac.mju.mp2019f.dbhelper;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

public class DBManager {

    DBHelper dbhlper;

    public DBManager(Context context ) {
        this.dbhlper = new DBHelper(context)
    }

    public long insertData ( String category, String name ) {

        SQLiteDatabase db = dbhlper.getWritableDatabase();

        ContentValues cv = new ContentValues();
        cv.put( "category", category );
        cv.put( "name", name );
        cv.put( "done", "N");

        long id = db.insert( "shoppinglist", null, cv );
        db.close();
        return id;

    }

    public class DBHelper extends SQLiteOpenHelper {


        public DBHelper(Context context) {
            super(context, "mydbhelper", null, 1);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {

            db.execSQL("CREATE TABLE shoppinglist (sID INTEGER PRIMARY KEY AUTOINCREMENT, " +
                    " category VCHAR(20), name TEXT, done CHAR(1) )");
        }

        @Override
        public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

        }
    };

}
